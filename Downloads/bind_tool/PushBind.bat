@echo off
cls
setlocal enabledelayedexpansion

for /f "delims=" %%i in ('hdc_std list targets') do (
echo %%i | findstr Empty
if !ERRORLEVEL!==1 call:PushFile %%i
)

pause
EXIT /b 0

:PushFile

echo ------Device: %1

hdc_std -t %1 shell "rm -rf /data/bind"

hdc_std -t %1 shell "mkdir /data/bind"

hdc_std -t %1 file send %cd%\deviceauth_ipc /data/bind/

hdc_std -t %1 shell "chmod a+x /data/bind/deviceauth_ipc"

hdc_std -t %1 file send %cd%\bindclient /data/bind/

hdc_std -t %1 shell "chmod a+x /data/bind/bindclient"

hdc_std -t %1 file send %cd%\bindserver /data/bind/

hdc_std -t %1 shell "chmod a+x /data/bind/bindserver"

goto:eof
