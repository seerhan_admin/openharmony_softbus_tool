/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TRANSMISSION_H
#define TRANSMISSION_H

#ifdef __cplusplus
extern "C" {
#endif

void T_CreateSessionServer(void);
void T_RemoveSessionServer(void);
void T_OpenSession(void);
void T_CloseSession(void);
void T_SetFileSendListener(void);
void T_SetFileReceiveListener(void);
void T_SendMessage(void);
void T_SendBytes(void);
void T_SendStream(void);
void T_SendFile(void);
#ifdef __cplusplus
}
#endif
#endif /* TRANSMISSION_H */
